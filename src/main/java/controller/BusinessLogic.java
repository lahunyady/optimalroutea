package controller;

import connection.PostgreSQLConnection;
import database.PostgreDataDAO;
import entities.LineRoute;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Optimization logic
 **/
public abstract class BusinessLogic {
    private static final int DEGREE_TO_REMOVE = 2;

    private static HashMap<Long, Integer> pointMap = new HashMap<>();
    private static List<LineRoute> inputLineList;
    private static List<LineRoute> outputLineList;

    public static void Optimize() {
        System.out.println("Starting Initialization");
        Init();
        System.out.println("Starting CountPointOccurrences");
        CountPointOccurrences();
        System.out.println("Copying records to output list excluding ones with 2 degree");
        CopyUsefulRecords(DEGREE_TO_REMOVE);
        System.out.println("Starting Upload");
        PostgreDataDAO.lineDAO.Upload(outputLineList);
        PostgreSQLConnection.Dispose();

    }

    /**
     * Initialize variables used while optimizing.
     **/
    private static void Init() {
        inputLineList = PostgreDataDAO.lineDAO.getRecords();
        outputLineList = new ArrayList<>();

        /**to get copy in memory not just reference
         * least amount of db request**/
        for (LineRoute item :
                inputLineList) {
            outputLineList.add(new LineRoute(
                    item.getId(),
                    item.getPoint_start(),
                    item.getPoint_end()
            ));
        }
    }

    /**
     * Iterate through inputLineList, and add point_start, point_add to HashMap
     **/
    private static void CountPointOccurrences() {
        for (LineRoute item : inputLineList) {
            AddPointToHashMap(item.getPoint_start());
            AddPointToHashMap(item.getPoint_end());
        }
    }

    /**
     * If the key already exists, increase occurrence
     **/
    private static void AddPointToHashMap(long point) {
        if (pointMap.containsKey(point)) {
            pointMap.put(point, pointMap.get(point) + 1);
        } else {
            pointMap.put(point, 1);
        }
    }

    /**
     * Iterate through HashMap,
     * and ChainOut the points which do not need to be registered
     **/
    private static void CopyUsefulRecords(int degreeToRemove) {
        int i = 1;
        for (long key :
                pointMap.keySet()) {
            if (pointMap.get(key) == degreeToRemove) {

                ChainOut(key);
                System.out.println(i++ + "  ROW REMOVED FROM LIST OF OUTTABLE");
            }

        }
    }

    /**
     * get the point ids different from the one waiting to be deleted.
     * save the line id of the first record
     * delete the lines from the list which contain idToRemove points
     * insert a new line to the list without idToRemove point
     **/
    private static void ChainOut(long idToRemove) {

        List<LineRoute> recordsToRemove = RecordsToModify(idToRemove);
        long newStartId = -1;
        long newEndId = -1;
        long newLineId = -1;

        Iterator<LineRoute> iter = recordsToRemove.iterator();

        while (iter.hasNext()) {
            LineRoute p = iter.next();
            if (p.getPoint_start() == idToRemove || p.getPoint_end() == idToRemove) {
                if (newStartId == -1) {
                    newStartId = p.getPoint_start() == idToRemove ? p.getPoint_end() : p.getPoint_start();
                    newLineId = p.getId();
                } else {
                    newEndId = p.getPoint_start() == idToRemove ? p.getPoint_end() : p.getPoint_start();
                }
                outputLineList.remove(p);

            }
        }
        outputLineList.add(new LineRoute(
                newLineId,
                newStartId,
                newEndId
        ));


    }

    /**
     * lines containing idToRemove id points.
     **/
    private static List<LineRoute> RecordsToModify(long idToRemove) {
        List<LineRoute> recordsToRemove = outputLineList
                .stream()
                .filter(lineRoute ->
                        lineRoute.getPoint_start() == idToRemove ||
                                lineRoute.getPoint_end() == idToRemove)
                .collect(Collectors.toList());
        return recordsToRemove;
    }
}
