package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class used by the BusinessLogic Layer
 **/
public class LineRoute {
    private long id;
    private long point_start;
    private long point_end;

    /**
     * Constructor
     **/
    public LineRoute(long id, long point_start, long point_end) {
        this.id = id;
        this.point_start = point_start;
        this.point_end = point_end;
    }

    public long getId() {
        return id;
    }

    public long getPoint_start() {
        return point_start;
    }

    public long getPoint_end() {
        return point_end;
    }

    /**
     * Convert the object received from the database to a BusinessLogic element
     **/
    public static LineRoute Convert(ResultSet resultSet) throws SQLException {
        return new LineRoute(
                resultSet.getInt(1),
                resultSet.getInt(2),
                resultSet.getInt(3)
        );
    }

    /**
     * Returns a string, which executed in insert sql command registers the entity as a db record
     **/
    public String InsertString() {
        return "(" + this.id + ", " + this.point_start + ", " + this.point_end + ")";
    }
}
