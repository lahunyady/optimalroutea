import controller.BusinessLogic;

public class Main {
    /**
     * Entry Point
     **/
    public static void main(String[] args) {
        BusinessLogic.Optimize();
    }
}

