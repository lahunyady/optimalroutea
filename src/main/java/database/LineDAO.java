package database;

import connection.PostgreSQLConnection;
import entities.LineRoute;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Conversion layer between the connection and BusinessLogic layer
 **/
public class LineDAO extends PostgreDataDAO implements ILineDAO {
    public static LineDAO lineDAO;

    /**
     * on first run call parent constructor to establish connection.
     **/
    public static PostgreDataDAO getInstance() {
        if (lineDAO == null) {
            lineDAO = new LineDAO();
        }
        return lineDAO;
    }

    /**
     * Calls parent constructor
     **/
    private LineDAO() {
        super();
    }

    /**
     * Gets all of the records from the input table, and converts it to classes used by BusinessLogic layer
     **/
    public ArrayList<LineRoute> getRecords() {
        ArrayList<LineRoute> entityList = new ArrayList<>();
        String selectAll =
                "SELECT * FROM public." + TABLE_TEST + ";";
        ResultSet resultSet = PostgreSQLConnection.QueryForResult(selectAll);
        try {
            while (resultSet.next()) {
                entityList.add(LineRoute.Convert(resultSet));
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entityList;
    }

    /**
     * Converts the list to upload into sql querys and executes it one by one.
     **/
    public void Upload(List<LineRoute> outputLineList) {
        CreateOutputTable();
        String query = "";
        int i = 1;
        for (LineRoute item :
                outputLineList) {
            query = "INSERT INTO public." + TABLE_OUTPUT + " VALUES " + item.InsertString() + ";";
            PostgreSQLConnection.Query(query);
            System.out.println(i++ + " / " + outputLineList.size() + " ROW INSERTED IN REPOSITORY ");
        }
    }

    /**
     * deletes the output table if already exists, and creates a new one
     **/
    private void CreateOutputTable() {
        String query = "DROP TABLE IF EXISTS public." + TABLE_OUTPUT + ";";
        PostgreSQLConnection.Query(query);
        query = "CREATE TABLE public." + TABLE_OUTPUT + "( " + COLUMN_ID + " bigint NOT NULL, " + COLUMN_POINT_START + " bigint, " + COLUMN_POINT_END + " bigint, "
                + " CONSTRAINT outdb_pkey PRIMARY KEY (" + COLUMN_ID + "));";
        PostgreSQLConnection.Query(query);
    }
}
