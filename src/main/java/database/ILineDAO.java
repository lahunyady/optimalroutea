package database;

/**
 * Strings used in LineDAO
 */
public interface ILineDAO {
    String COLUMN_ID = "id";
    String COLUMN_POINT_START = "point_start";
    String COLUMN_POINT_END = "point_end";
}
