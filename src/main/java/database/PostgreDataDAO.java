package database;

import connection.PostgreSQLConnection;

public class PostgreDataDAO implements IPostgreDataDAO {
    public static LineDAO lineDAO = (LineDAO) LineDAO.getInstance();

    /**
     * Initialize database connection with authentication data
     **/
    protected PostgreDataDAO() {
        PostgreSQLConnection.init(USERNAME, PASSWORD);
    }
}
