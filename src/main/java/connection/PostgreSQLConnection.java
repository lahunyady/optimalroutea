package connection;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Layer communicating with db through sql query's
 **/
public abstract class PostgreSQLConnection {
    private static Logger logger = Logger.getLogger(PostgreSQLConnection.class.getName());
    private static final String CONNECTION_URL = "jdbc:postgresql://localhost:5432/postgres";

    private static Connection CONNECTION;
    private static Statement STATEMENT;

    /**
     * Establish connection
     **/
    public static void init(String user, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            CONNECTION = DriverManager.getConnection(CONNECTION_URL, user, password);
            STATEMENT = CONNECTION.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARNING, e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run a query with no response such as insert, update, delete
     **/
    public static void Query(String query) {
        try {
            STATEMENT.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run a query with response such as select
     **/
    public static ResultSet QueryForResult(String query) {
        ResultSet resultSet = null;
        try {
            resultSet = STATEMENT.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static void Dispose(){
        try {
            STATEMENT.close();
            CONNECTION.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}